#!/usr/bin/env python
# -*- coding: utf-8 -*-

from estudiante import Estudiante
from Parqueadero import Parqueadero
from estudiante import Estudiante

class Cli(object):
	def __init__(self):
		self.seguir = True
		self.parqueadero=Parqueadero()

	def separador(self):
		print ("------------------")

	def bienvenida(self):
		self.separador()
		print ("Bienvenido a Parquedero Estudiantes")
		self.separador()

	def pedirNuevoEstudiante(self):
		self.separador()
		print ("Nuevo estudiante")
		self.separador()

	def ingreso(self):
		self.separador()
		print ("Ingreso de estudiante")
		print ("Ingrese nombre: ")
		nombre_est=input()
		print ("Ingrese codigo: ")
		codigo_est=input()
		print ("Ingrese numero de placa: ")
		placa_est=input()
		self.separador()
		return Estudiante(nombre_est, codigo_est, placa_est)

	def atencion(self):
		self.separador()
		print ("Atendiendo a estudiante")
		estudiante = self.parqueadero.atender()
		print ("Nombre del estudiante: "+estudiante.Nombre)
		print ("Codigo del estudiante: "+estudiante.Codigo)
		print ("Placa del estudiante: "+estudiante.Placa)
		self.separador()

	def salida(self):
		self.separador()
		print ("Gracias por usar el servicio")
		self.separador()

	def menu(self):
		self.separador()
		print ("1. Inscribir")
		print ("2. Atender")
		print ("3. Cantidad en Cola")
		print ("4. Salir")
		print("Que desea hacer?:")
		return int(input())

	def iterador(self):
		while (self.seguir):
			x = self.menu()
			if (x == 1):
				self.parqueadero.inscribir(self.ingreso())
				print ("Hecho correctamente!!")
			elif (x == 2):
				self.atencion()
			elif (x == 3):
				print ("La cantidad de dato/s son/es: "+str(self.parqueadero.cantidad()))
			else:
				self.seguir = False
		self.salida()

if __name__ == "__main__":
    run=Cli()
    run.iterador()