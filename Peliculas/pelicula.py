# -*- coding: utf-8 -*-
# Clase pelicula

class Pelicula:
    nombre = ""
    año = 0
    genero = ""
    actor = ""

    def __init__(self, nombre, año, genero, actor):
        self.nombre = nombre
        self.año = año
        self.genero = genero
        self.actor = actor
